import 'package:flutter/material.dart';
import '../pages/tabs.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'AppBar',
        theme: ThemeData(primarySwatch: Colors.green),
        home: const HomePage());
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  // 生命周期函数
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // 最左侧图标
          leading: IconButton(
            onPressed: () {},
            icon: const Icon(Icons.menu_sharp),
          ),
          // 最右侧的图标
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.search),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.more_horiz_sharp),
            )
          ],
          bottom: TabBar(
            // 是否可以滚动
            isScrollable: true,
            // 指示器颜色
            indicatorColor: Colors.white,
            // 指示器宽度
            indicatorWeight: 5,
            // 指示器padding
            indicatorPadding: const EdgeInsets.all(5),
            controller: _tabController,
            // 指示器的大小计算方式(与tab或者文字)
            indicatorSize: TabBarIndicatorSize.tab,
            // 所有label的颜色
            labelColor: Colors.red,
            // 配置所有label样式
            labelStyle: const TextStyle(fontSize: 18),
            unselectedLabelStyle: const TextStyle(fontSize: 15),
            unselectedLabelColor: Colors.white,
            tabs: const [
              Tab(
                child: Text("关注"),
              ),
              Tab(
                child: Text("热门"),
              ),
              Tab(
                child: Text("视频"),
              ),
            ],
          ),
          // 背景颜色
          backgroundColor: Colors.grey,
          title: const Text("appbar"),
        ),
        body: TabBarView(
          controller: _tabController,
          children: [
            const Center(
              child: Icon(Icons.notification_add_outlined),
            ),
            ListView(
              children: const [
                ListTile(
                  title: Text("我是热门列表"),
                ),
              ],
            ),
            ListView(
              children: const [
                ListTile(
                  title: Text("我是视频列表"),
                ),
              ],
            ),
          ],
        ));
  }
}
