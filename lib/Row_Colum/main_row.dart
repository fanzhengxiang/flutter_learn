import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "行和列",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("padding组件")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.grey,
        child: Row(
          // 主轴排列方式, 外部没有容器的时候是自适应的
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          // 纵轴排列方式
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            IconContainer(Icons.home_outlined, color: Colors.red),
            IconContainer(Icons.search, color: Colors.green),
            IconContainer(Icons.local_airport, color: Colors.pinkAccent),
          ],
        ));
  }
}

class IconContainer extends StatelessWidget {
  Color color;
  IconData icon;

  IconContainer(this.icon, {Key? key, required this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: 80,
      height: 80,
      color: color,
      child: Icon(
        icon,
        size: 35,
        color: Colors.white,
      ),
    );
  }
}
