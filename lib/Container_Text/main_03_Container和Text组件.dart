import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
        appBar: AppBar(
            title: const Text(
          "Container组件和Text组件",
          style: TextStyle(color: Colors.black),
        )),
        body: const Column(
          children: [MyApp(), MyButton(), MyText()],
        )),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: Container(
        // 容器内元素的位置
        alignment: Alignment.center,
        width: 200,
        height: 200,
        padding: const EdgeInsets.all(15),
        margin: const EdgeInsets.fromLTRB(0, 100, 0, 0),
        // x,y方向进行位移
        // transform: Matrix4.translationValues(-40, 15, 0),
        // 沿着x,y,z方向进行旋转
        transform: Matrix4.rotationZ(0.3),
        decoration: BoxDecoration(
            // 盒子背景颜色
            color: Colors.green,
            // 边框
            border: Border.all(color: Colors.pinkAccent, width: 5),
            // 圆角
            borderRadius: BorderRadius.circular(50),
            // 阴影
            boxShadow: const [
              BoxShadow(color: Colors.blue, blurRadius: 20),
            ],
            // 渐变
            gradient: const LinearGradient(
                colors: [Colors.red, Colors.yellow],
                begin: Alignment.center,
                end: Alignment.centerLeft)),
        child: const Text(
          "Container组件",
          style: TextStyle(color: Colors.white, fontSize: 30),
        ),
      ),
    );
  }
}

class MyButton extends StatelessWidget {
  const MyButton({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      alignment: Alignment.center,
      width: 200,
      height: 60,
      margin: const EdgeInsets.all(20),
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.pinkAccent,
            width: 5,
          ),
          borderRadius: BorderRadius.circular(30),
          color: Colors.blue),
      child: const Text(
        "点击按钮",
        style: TextStyle(
            color: Colors.white, fontSize: 25, fontWeight: FontWeight.w200),
      ),
    );
  }
}

class MyText extends StatelessWidget {
  const MyText({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 40, 0, 0),
      width: 200,
      height: 200,
      decoration: BoxDecoration(
          color: Colors.brown, borderRadius: BorderRadius.circular(10)),
      child: const Text(
        "文本组件文本组件文本组件文本组件文本组件文本组件文本组件文本组件文本组件",
        textAlign: TextAlign.left,
        // 最大行数
        maxLines: 1,
        // 溢出时显示方式
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
            color: Colors.pinkAccent,
            // 文本装饰
            decoration: TextDecoration.underline,
            // 文本装饰颜色
            decorationColor: Colors.black,
            // 文本装饰类型
            decorationStyle: TextDecorationStyle.dotted,
            // 字母间距
            letterSpacing: 5,
            fontSize: 30,
            fontWeight: FontWeight.w700,
            fontStyle: FontStyle.italic),
      ),
    );
  }
}
