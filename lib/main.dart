import 'package:flutter/material.dart';

import './routers/routers.dart';
void main() {
  runApp(MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {

  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'AppBar',
        theme: ThemeData(primarySwatch: Colors.green),
        initialRoute: '/',
        // 2.配置onGenerateRoute
        onGenerateRoute: onGenerateRoute);
  }
}
