import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "行和列",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("stack组件")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    // 获取设备的宽高
    final size = MediaQuery.of(context).size;
    return Stack(
      children: [
        ListView(
          padding: const EdgeInsets.only(top: 50),
          children: const [
            ListTile(
              title: Text("我是一个列表组件1"),
            ),
            ListTile(
              title: Text("我是一个列表组件2"),
            ),
            ListTile(
              title: Text("我是一个列表组件3"),
            ),
            ListTile(
              title: Text("我是一个列表组件4"),
            ),
            ListTile(
              title: Text("我是一个列表组件5"),
            ),
            ListTile(
              title: Text("我是一个列表组件6"),
            ),
            ListTile(
              title: Text("我是一个列表组件7"),
            ),
            ListTile(
              title: Text("我是一个列表组件8"),
            ),
            ListTile(
              title: Text("我是一个列表组件9"),
            ),
            ListTile(
              title: Text("我是一个列表组件10"),
            ),
            ListTile(
              title: Text("我是一个列表组件11"),
            ),
            ListTile(
              title: Text("我是一个列表组件12"),
            ),
            ListTile(
              title: Text("我是一个列表组件13"),
            ),
            ListTile(
              title: Text("我是一个列表组件14"),
            ),
            ListTile(
              title: Text("我是一个列表组件15"),
            ),
          ],
        ),
        Positioned(
            bottom: 0,
            left: 0,
            width: size.width,
            height: 40,// 配置子元素的宽度和高度,无法使用double.infinity
            child: Container(
              alignment: Alignment.center,
              height: 44,
              color: Colors.pinkAccent,
              child: const Text(
                "二级导航",
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ))
      ],
    );
  }
}
