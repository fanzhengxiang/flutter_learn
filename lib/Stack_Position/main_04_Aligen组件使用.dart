import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "行和列",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("stack组件")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    // 获取设备的宽高
    final size = MediaQuery.of(context).size;
    return Container(
        height: 300,
        width: 300,
        color: Colors.red,
        child: const Align(
          alignment: Alignment(0,1),
          child: Text("Align使用"),
        ));
  }
}
