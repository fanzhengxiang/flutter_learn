import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "行和列",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("stack组件")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: 300,
      height: 300,
      color: Colors.pinkAccent,
      child: Stack(// 相对于外部容器进行定位的,如果没有外部容器,就相对于整个屏幕定位
        children: [
          Positioned(
              left: 20,
              bottom: 20,
              child: Container(
                width: 100,
                height: 100,
                color: Colors.green,
              )),
          const Positioned(right: 0, top: 100, child: Text("你好 flutter"))
        ],
      ),
    );
  }
}
