import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
      appBar: AppBar(
        title: const Text("加载图片"),
      ),
      body: const Center(
          child: Column(
        children: [MyApp(), SizedBox(height: 20), Circular()],
      )),
    ),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 120, 0, 0),
      width: 200,
      height: 200,
      // alignment: Alignment.center,
      // 使用BorderRadius实现圆形图片
      decoration: BoxDecoration(
          color: Colors.green,
          borderRadius: BorderRadius.circular(100),
          image: const DecorationImage(
              image: NetworkImage(
                  'https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg'),
              fit: BoxFit.cover)),
    );
  }
}

class Circular extends StatelessWidget {
  const Circular({super.key});

  @override
  Widget build(BuildContext context) {
    // 使用ClipOval实现圆形图片
    return ClipOval(
      child: Image.network(
        "https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg",
        width: 200,
        height: 200,
        fit: BoxFit.cover,
      ),
    );
  }
}
