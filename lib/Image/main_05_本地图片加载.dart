import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
      appBar: AppBar(
        title: const Text("加载图片"),
      ),
      body: const Center(
          child: Column(
        children: [
          SizedBox(
            height: 100,
          ),
          MyApp(),
          SizedBox(height: 20)
        ],
      )),
    ),
  ));
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 200,
      // alignment: Alignment.center,
      // 使用BorderRadius实现圆形图片
      decoration: const BoxDecoration(
        color: Colors.green,
      ),
      child: Image.asset(
        "images/3.0x/head.jpg",
        fit: BoxFit.cover,
      ),
    );
  }
}
