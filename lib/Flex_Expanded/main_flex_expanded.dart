import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "行和列",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("padding组件")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: [
        SizedBox(
            height: 500,
            child: Column(
              // 这里也可以用Row和Colum
              children: [
                // flex属性需要写在第一个 Expanded中设置宽高是没有效果的
                Expanded(
                    flex: 1,
                    child: Container(
                      color: Colors.black,
                    )),
                Expanded(
                    flex: 1,
                    child: Row(
                      children: [
                        Expanded(
                            flex: 2,
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              height: 250,
                              child: Image.network(
                                "https://www.itying.com/images/flutter/1.png",
                                fit: BoxFit.cover,
                              ),
                            )),
                        Expanded(
                            flex: 1,
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              height: 250,
                              child: Column(
                                children: [
                                  Expanded(
                                      flex: 1,
                                      child: SizedBox(
                                        width: double.infinity,
                                        child: Image.network(
                                          "https://www.itying.com/images/flutter/2.png",
                                          fit: BoxFit.cover,
                                        ),
                                      )),
                                  Expanded(
                                      flex: 2,
                                      child: SizedBox(
                                        width: double.infinity,
                                        child: Image.network(
                                          "https://www.itying.com/images/flutter/3.png",
                                          fit: BoxFit.cover,
                                        ),
                                      )),
                                ],
                              ),
                            ))
                      ],
                    ))
              ],
            ))
      ],
    );
  }
}

class IconContainer extends StatelessWidget {
  Color color;
  IconData icon;

  IconContainer(this.icon, {Key? key, required this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: 80,
      height: 80,
      color: color,
      child: Icon(
        icon,
        size: 35,
        color: Colors.white,
      ),
    );
  }
}
