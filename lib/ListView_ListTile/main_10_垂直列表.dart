import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.pink),
      home: Scaffold(
        appBar: AppBar(title: const Text("列表组件")),
        body: ListView(
          padding: const EdgeInsets.all(10),
          children: <Widget>[
            Image.network(
              "https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg",
              fit: BoxFit.cover,
            ),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: const Text(
                  "第一张图片",
                  style: TextStyle(fontSize: 20, color: Colors.green),
                  textAlign: TextAlign.center,
                )),
            Image.network(
              "https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg",
              fit: BoxFit.cover,
            ),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: const Text(
                  "第一张图片",
                  style: TextStyle(fontSize: 20, color: Colors.green),
                  textAlign: TextAlign.center,
                )),
            Image.network(
              "https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg",
              fit: BoxFit.cover,
            ),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: const Text(
                  "第一张图片",
                  style: TextStyle(fontSize: 20, color: Colors.green),
                  textAlign: TextAlign.center,
                )),
            Image.network(
              "https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg",
              fit: BoxFit.cover,
            ),
            Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                child: const Text(
                  "第一张图片",
                  style: TextStyle(fontSize: 20, color: Colors.green),
                  textAlign: TextAlign.center,
                )),
          ],
        ),
      ),
    );
  }
}
