import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.pink),
      home: Scaffold(
        appBar: AppBar(title: const Text("列表组件")),
        body: ListView(
          children: const <Widget>[
            ListTile(
                leading: Icon(
                  Icons.home,
                  size: 35,
                  color: Colors.red,
                ),
                title: Text("首页")),
            Divider(),
            ListTile(
                leading: Icon(
                  Icons.assessment,
                  size: 35,
                  color: Colors.green,
                ),
                title: Text("全部订单"),
              trailing: Icon(Icons.access_alarm),
            ),
            Divider(),
            ListTile(
                leading: Icon(
                  Icons.payment,
                  size: 35,
                  color: Colors.pink,
                ),
                title: Text("代付款")),
            Divider(),
            ListTile(
                leading: Icon(
                  Icons.favorite,
                  size: 35,
                  color: Colors.lightGreen,
                ),
                title: Text("我的收藏")),
            Divider(),
            ListTile(
              leading: Icon(
                Icons.people,
                size: 35,
                color: Colors.black45,
              ),
              title: Text("在线客服"),
              trailing: Icon(Icons.keyboard_double_arrow_right),
            ),
            Divider(),
          ],
        ),
      ),
    );
  }
}
