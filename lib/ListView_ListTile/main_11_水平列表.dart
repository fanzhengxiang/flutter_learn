import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.pink),
      home: Scaffold(
        appBar: AppBar(title: const Text("列表组件")),
        body: SizedBox(
          height: 300,
          child: ListView(
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.all(10),
            children: <Widget>[
              Container(
                height: 250,
                // 直接在ListView中使用Container,水平方向高度自适应,没有效果
                padding: const EdgeInsets.all(10),
                width: 180,
                // 直接在ListView中使用Container,垂直方向宽度自适应,没有效果
                decoration: const BoxDecoration(color: Colors.white70),
                child: Column(
                  children: [
                    Image.network(
                      "https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg",
                      fit: BoxFit.cover,
                    ),
                    const Text("1999元",style: TextStyle(color: Colors.black),textAlign: TextAlign.center,)
                  ],
                ),
              ),
              Container(
                height: 250,
                padding: const EdgeInsets.all(10),
                width: 180,
                decoration: const BoxDecoration(color: Colors.blueGrey),
              ),
              Container(
                height: 250,
                padding: const EdgeInsets.all(10),
                width: 180,
                decoration: const BoxDecoration(color: Colors.red),
              ),
              Container(
                height: 250,
                padding: const EdgeInsets.all(10),
                width: 180,
                decoration: const BoxDecoration(color: Colors.green),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
