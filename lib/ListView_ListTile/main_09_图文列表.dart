import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.pink),
      home: Scaffold(
        appBar: AppBar(title: const Text("列表组件")),
        body: ListView(
          padding: const EdgeInsets.all(10),
          children: <Widget>[
            ListTile(
              leading: Image.network(
                "https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg",
                fit: BoxFit.cover,
              ),
              title: const Text("美总统参选人承认美国在乌克兰有生物实验室"),
              subtitle: const Text("肯尼迪在社交媒体平台的一段采访视频中表示"),
            ),
            const Divider(),
            ListTile(
              trailing: Image.network(
                "https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg",
                fit: BoxFit.cover,
              ),
              leading: Image.network(
                "https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg",
                fit: BoxFit.cover,
              ),
              title: const Text("美总统参选人承认美国在乌克兰有生物实验室"),
              subtitle: const Text("肯尼迪在社交媒体平台的一段采访视频中表示"),
            )
          ],
        ),
      ),
    );
  }
}
