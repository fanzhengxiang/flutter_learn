import 'package:flutter/material.dart';
import '../myFont.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.pink),
      home: Scaffold(
        appBar: AppBar(title: const Text("列表组件")),
        body: ListView(
          children: const <Widget>[
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
            ListTile(title: Text("我是一个列表元素")),
            Divider(),
          ],
        ),
      ),
    );
  }
}
