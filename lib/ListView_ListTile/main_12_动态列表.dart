import 'package:flutter/material.dart';
import '../data/listData.dart';

void main() {
  runApp(MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  List<String> list = [];

  MyApp({super.key}) {
    for (int i = 0; i < 30; i++) {
      list.add("我是第$i条数据");
    }
  }

  // for循环生成动态列表
  List<Widget> _initListData1() {
    List<Widget> list = [];
    for (int i = 0; i < 20; i++) {
      list.add(
        ListTile(
          title: Text(
            "我是一个列表---$i",
            style: const TextStyle(color: Colors.green),
          ),
        ),
      );
    }
    return list;
  }

  // 使用外部数据生成动态列表
  List<Widget> _initListData2() {
    List<Widget> list = [];
    for (int i = 0; i < listData.length; i++) {
      var data = listData[i];
      list.add(ListTile(
        leading: Image.network(data["imgUrl"]),
        title: Text(data["title"]),
        subtitle: Text(data["author"]),
      ));
    }
    return list;
  }

  // 使用map方式放回一个动态列表
  List<Widget> _initListData3() {
    var tempList = listData.map((e) {
      return ListTile(
        leading: Image.network(
          e["imgUrl"],
          fit: BoxFit.cover,
        ),
        title: Text(e['title']),
        subtitle: Text(e['author']),
      );
    });
    return tempList.toList();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("动态列表组件")),
          body: ListView.builder(
              itemCount: listData.length,
              // 使用ListView.builder生成动态列表
              itemBuilder: (context, index) {
                var data = listData[index];
                return ListTile(
                  leading: Image.network(data['imgUrl']),
                  title: Text(data['title']),
                  subtitle: Text(data['author']),
                );
              }),
        ));
  }
}
