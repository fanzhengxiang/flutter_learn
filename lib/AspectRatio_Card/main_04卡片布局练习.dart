import 'package:flutter/material.dart';
import '../data/listData.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "行和列",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("卡片布局")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  List<Widget> _initCardData() {
    var tempList = listData.map((e) {
      return Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        margin: EdgeInsets.all(20),
        elevation: 20,
        child: Column(
          children: [
            AspectRatio(
              aspectRatio: 16 / 9,
              child: Image.network(
                e['imgUrl'],
                fit: BoxFit.cover,
              ),
            ),
            ListTile(
              // ClipOval生成圆形图片
              leading: ClipOval(
                child: Image.network(
                  e['imgUrl'],
                  fit: BoxFit.cover,
                  width: 50,
                  height: 50,
                ),
              ),
              title: Text(e['title']),
              subtitle: Text(e['author']),
              trailing: CircleAvatar(
                backgroundImage: NetworkImage(e['imgUrl']),
              ), //CircleAvatar生成圆形图片
            )
          ],
        ),
      );
    });

    return tempList.toList();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: _initCardData(),
    );
  }
}
