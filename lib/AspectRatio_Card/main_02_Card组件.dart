import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "行和列",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("Aspect组件")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),// 配置阴影效果
            color: Colors.white70,
            margin: const EdgeInsets.all(10),
            shadowColor: Colors.green,
            elevation: 20,
            // 卡片阴影深度
            child: const Column(
              children: [
                ListTile(
                  title: Text(
                    '张三',
                    style: TextStyle(fontSize: 25),
                  ),
                  subtitle: Text("高级java开发工程师"),
                ),
                Divider(),
                ListTile(title: Text('电话: 15828684684')),
                ListTile(title: Text('地址: 成都')),
              ],
            )),
        const SizedBox(
          height: 30,
        ),
        const Card(
            margin: EdgeInsets.all(10),
            elevation: 10,
            child: Column(
              children: [
                ListTile(
                  title: Text(
                    '李四',
                    style: TextStyle(fontSize: 25),
                  ),
                  subtitle: Text("高级运维开发工程师"),
                ),
                Divider(),
                ListTile(title: Text('电话: 15102886456')),
                ListTile(title: Text('地址: 上海')),
              ],
            )),
        Card(
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),// 配置阴影效果
            color: Colors.white70,
            margin: const EdgeInsets.all(10),
            shadowColor: Colors.green,
            elevation: 20,
            // 卡片阴影深度
            child: const Column(
              children: [
                ListTile(
                  title: Text(
                    '张三',
                    style: TextStyle(fontSize: 25),
                  ),
                  subtitle: Text("高级java开发工程师"),
                ),
                Divider(),
                ListTile(title: Text('电话: 15828684684')),
                ListTile(title: Text('地址: 成都')),
              ],
            )),
        const SizedBox(
          height: 30,
        ),
        const Card(
            margin: EdgeInsets.all(10),
            elevation: 10,
            child: Column(
              children: [
                ListTile(
                  title: Text(
                    '李四',
                    style: TextStyle(fontSize: 25),
                  ),
                  subtitle: Text("高级运维开发工程师"),
                ),
                Divider(),
                ListTile(title: Text('电话: 15102886456')),
                ListTile(title: Text('地址: 上海')),
              ],
            )),
      ],
    );
  }
}
