import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "行和列",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("Aspect组件")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          margin: const EdgeInsets.all(20),
          elevation: 20,
          child: Column(
            children: [
              AspectRatio(
                aspectRatio: 16 / 9,
                child: Image.network(
                  "https://www.itying.com/images/flutter/3.png",
                  fit: BoxFit.cover,
                ),
              ),
              ListTile(
                // ClipOval生成圆形图片
                leading: ClipOval(
                  child: Image.network(
                    "https://www.itying.com/images/flutter/3.png",
                    fit: BoxFit.cover,
                    width: 50,
                    height: 50,
                  ),
                ),
                title: const Text("xxxxxx"),
                subtitle: const Text("wwwwwwwwwww"),
                trailing: const CircleAvatar(backgroundImage: NetworkImage('https://www.itying.com/images/flutter/3.png'),),//CircleAvatar生成圆形图片
              )
            ],
          ),
        )
      ],
    );
  }
}
