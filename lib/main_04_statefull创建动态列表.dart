import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.pink),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final List<String> _list=[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("有状态组件")),
        body: ListView(
          children: _list.map((e){
            return ListTile(title: Text(e),);
          }).toList(),
        ),
        floatingActionButton: FloatingActionButton(onPressed: (){
          setState(() {
            _list.add("我是第${_list.length+1}个列表元素");
          });
        },child: const Icon(Icons.thumb_up),)
    );
  }
}
