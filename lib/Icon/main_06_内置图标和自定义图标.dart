import 'package:flutter/material.dart';
import '../myFont.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.pink),
      home: Scaffold(
        appBar: AppBar(title: const Text("图标组件")),
        body: const Column(
          children: [
            Icon(
              Icons.home_outlined,
              size: 50,
              color: Colors.blue,
            ),
            SizedBox(height: 40),
            Icon(Icons.search,size: 50,color: Colors.green,),
            SizedBox(height: 40),
            Icon(Icons.category_rounded,size: 50,color: Colors.green,),
            SizedBox(height: 40),
            Icon(AiresFont.wechat,color: Colors.greenAccent,size: 50),
            SizedBox(height: 40),
            Icon(AiresFont.alipay,color: Colors.blue,size: 50,),
            SizedBox(height: 40),
            Icon(AiresFont.sony,color: Colors.red,size: 50,),
            SizedBox(height: 40),
            Icon(AiresFont.adidas,color: Colors.blueGrey,size: 50,),
            SizedBox(height: 40),
            Icon(AiresFont.location,color: Colors.blueGrey,size: 50,),

          ],
        ),
      ),
    );
  }
}
