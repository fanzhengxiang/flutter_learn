import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "行和列",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("按钮组件")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
                flex: 3,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: ElevatedButton.icon(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.indigoAccent)),
                      onPressed: () {},
                      icon: const Icon(Icons.access_alarm),
                      label: const Text("自适应按钮左")),
                )),
            Expanded(
                flex: 2,
                child: ElevatedButton.icon(
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.green),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15)))),
                    onPressed: () {},
                    icon: const Icon(Icons.search),
                    label: const Text("自适应按钮右"))),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            // 使用外层容器配置圆形按钮
            SizedBox(
              width: 100,
              height: 100,
              child: ElevatedButton(
                style: ButtonStyle(
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50)))),
                onPressed: () {},
                child: const Text("圆形按钮"),
              ),
            )
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            // 使用CircleBorder配置圆形按钮
            SizedBox(
              width: 100,
              height: 100,
              child: ElevatedButton(
                onPressed: () {},
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all(Colors.lightGreen),
                    foregroundColor: MaterialStateProperty.all(Colors.black45),
                    shape: MaterialStateProperty.all(const CircleBorder(
                        side: BorderSide(color: Colors.orange, width: 15)))),
                child: const Text("圆形按钮2"),
              ),
            )
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Row(
          // 修改OutlinedButton按钮的边框
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            OutlinedButton(
                onPressed: () {},
                style: ButtonStyle(
                    // 配置背景颜色
                    backgroundColor: MaterialStateProperty.all(Colors.cyan),
                    // 配置前景颜色
                    foregroundColor: MaterialStateProperty.all(Colors.white),
                    // 配置圆角
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10))),
                    // 配置边框
                    side: MaterialStateProperty.all(const BorderSide(color: Colors.red,width: 5))),
                child: const Text("边框按钮"))
          ],
        )
      ],
    );
  }
}
