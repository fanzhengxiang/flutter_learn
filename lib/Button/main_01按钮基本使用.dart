import 'package:flutter/material.dart';
import '../data/listData.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "行和列",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("按钮组件")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            ElevatedButton(onPressed: () {}, child: const Text("普通按钮")),
            TextButton(onPressed: () {}, child: const Text("文本按钮")),
            const OutlinedButton(
              onPressed: null,
              child: Text("边框按钮"),
            ),
            IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.thumb_up,
                  color: Colors.green,
                ))
          ],
        ),
        const SizedBox(
          height: 40,
        ),
        Column(
          children: [
            ElevatedButton.icon(
              icon: const Icon(Icons.access_alarm),
              onPressed: () {},
              label: const Text("普通按钮带图标"),
            ),
            TextButton.icon(
                onPressed: () {},
                icon: const Icon(Icons.ac_unit),
                label: const Text("文本按钮带图标")),
            SizedBox(
              width: 200,
              height: 50,
              child: OutlinedButton.icon(
                  // 配置按钮样式
                  style: ButtonStyle(
                    // 设置按钮背景色
                    backgroundColor: MaterialStateProperty.all(Colors.white10),
                    // 设置文本颜色
                    foregroundColor: MaterialStateProperty.all(Colors.green),
                  ),
                  onPressed: () {},
                  icon: const Icon(Icons.send),
                  label: const Text("大按钮")),
            )
          ],
        )
      ],
    );
  }
}
