import 'package:flutter/material.dart';

class NewsPage extends StatefulWidget {
  final String title;

  final int aid;

  const NewsPage({super.key, this.title = "国内新闻", required this.aid});

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(onPressed: (){
        Navigator.of(context).pop();
      },child: const Icon(Icons.keyboard_return),),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body:  Center(child: Column(mainAxisAlignment: MainAxisAlignment.center,children: [
        ElevatedButton(onPressed: (){
          Navigator.of(context).pushNamed("/search");
        }, child: Text("跳转到搜索"))
      ],)),
    );
  }
}
