import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {

  Map arguments;

   FormPage({super.key,required this.arguments});

  @override
  State<FormPage> createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {

  @override
  void initState() {
    // TODO: implement initState
    print(widget.arguments);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("表单页面"),

      ),
      body: const Center(child: Text("表单内容"),),
    );
  }
}
