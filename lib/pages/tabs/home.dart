import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(15),
      children: [
        Row(
          children: [
            Text(
              "热搜",
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
        const Divider(
          color: Colors.black45,
        ),
        Wrap(
          spacing: 10,
          runSpacing: 5,
          children: [
            Button("女装", onPressed: () {}),
            Button("游戏", onPressed: () {}),
            Button("电脑", onPressed: () {}),
            Button("书籍", onPressed: () {}),
            Button("汽车", onPressed: () {}),
            Button("手机", onPressed: () {}),
          ],
        ),
        const SizedBox(
          height: 15,
        ),
        Row(
          children: [
            Text(
              "历史记录",
              style: Theme.of(context).textTheme.headlineSmall,
            ),
          ],
        ),
        const Divider(
          color: Colors.black45,
        ),
        const Column(
          children: [
            ListTile(
              title: Text("皮鞋"),
            ),
            Divider(
              color: Colors.white30,
            ),
            ListTile(
              title: Text("猫粮"),
            ),
            Divider(
              color: Colors.white30,
            ),
            ListTile(
              title: Text("羽绒服"),
            ),
          ],
        ),
        const SizedBox(
          height: 40,
        ),
        OutlinedButton.icon(
            onPressed: () {},
            icon: const Icon(Icons.delete_forever_outlined),
            label: const Text("清空历史记录1")),
        const SizedBox(
          height: 20,
        ),
        Row(
          children: [
            Expanded(
              flex: 1,
              child: OutlinedButton.icon(
                  onPressed: () {},
                  style: ButtonStyle(
                    foregroundColor: MaterialStateProperty.all(Colors.blue),
                  ),
                  icon: const Icon(Icons.delete_forever_outlined),
                  label: const Text("清空历史记录2")),
            )
          ],
        )
      ],
    );
  }
}
class Button extends StatelessWidget {
  String text;
  void Function()? onPressed;

  Button(this.text, {super.key, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ElevatedButton(
      onPressed: onPressed,
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(
              const Color.fromARGB(242, 255, 244, 244)),
          foregroundColor: MaterialStateProperty.all(Colors.black45)),
      child: Text(text),
    );
  }
}
