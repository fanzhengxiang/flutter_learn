import 'package:flutter/material.dart';
import 'package:flutter_learn/KeepAliveWrapper.dart';

class CategoryPage extends StatefulWidget {
  const CategoryPage({super.key});

  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = TabController(length: 9, vsync: this);
    // 获取当前tab的索引 会执行2次
    _tabController.addListener(() {
      if (_tabController.animation!.value == _tabController.index) {
        // 打印索引
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // 使用PreferredSize设置appBar高度
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(45),
        child: AppBar(
          // 阴影效果
          elevation: 5,
          backgroundColor: Colors.white,
          title: SizedBox(
            // 改tabBar的高度
            height: 30,
            child: TabBar(
              // 只能监听点击事件
              onTap: (index) {
                // 打印索引
              },
              labelStyle: const TextStyle(fontSize: 15),
              isScrollable: true,
              indicatorColor: Colors.red,
              unselectedLabelColor: Colors.black,
              indicatorSize: TabBarIndicatorSize.label,
              labelColor: Colors.red,
              controller: _tabController,
              tabs: const [
                Tab(
                  child: Text("每日推荐"),
                ),
                Tab(
                  child: Text("私人漫游"),
                ),
                Tab(
                  child: Text("歌单"),
                ),
                Tab(
                  child: Text("排行榜"),
                ),
                Tab(
                  child: Text("数字专辑"),
                ),
                Tab(
                  child: Text("有声书"),
                ),
                Tab(
                  child: Text("新歌"),
                ),
                Tab(
                  child: Text("收藏家"),
                ),
                Tab(
                  child: Text("游戏"),
                ),
              ],
            ),
          ),
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          // 缓存当前组件
          KeepAliveWrapper(
              child: ListView(
            children: const [
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
              ListTile(
                title: Text("热门新闻22"),
              ),
              ListTile(
                title: Text("热门新闻33"),
              ),
              ListTile(
                title: Text("热门新闻44"),
              ),
              ListTile(
                title: Text("热门新闻55"),
              ),
              ListTile(
                title: Text("热门新闻"),
              ),
            ],
          )),
          ListView(
            children: const [
              ListTile(
                title: Text("游戏直播"),
              )
            ],
          ),
          ListView(
            children: const [
              ListTile(
                title: Text("国外游戏"),
              )
            ],
          ),
          ListView(
            children: const [
              ListTile(
                title: Text("热门新闻"),
              )
            ],
          ),
          ListView(
            children: const [
              ListTile(
                title: Text("游戏直播"),
              )
            ],
          ),
          ListView(
            children: const [
              ListTile(
                title: Text("国外游戏"),
              )
            ],
          ),
          ListView(
            children: const [
              ListTile(
                title: Text("热门新闻"),
              )
            ],
          ),
          ListView(
            children: const [
              ListTile(
                title: Text("游戏直播"),
              )
            ],
          ),
          ListView(
            children: const [
              ListTile(
                title: Text("国外游戏"),
              )
            ],
          ),
        ],
      ),
    );
  }
}
