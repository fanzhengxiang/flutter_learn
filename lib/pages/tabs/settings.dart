import 'package:flutter/material.dart';
import './search.dart';
import './news.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return const SearchPage();
              }));
            },
            child: const Text("普通跳转")),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return const NewsPage(title: "国际新闻1",aid: 110,);
              }));
            },
            child: const Text("带参跳转")),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context,"/news");
            },
            child: const Text("带参到新闻页面")),
        const SizedBox(height: 20,),
        ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context,"/form",arguments: {"id":2342,"name":"kiki"});
            },
            child: const Text("命名路由跳转+传值"))
      ],
    ));
  }
}
