import 'package:flutter/material.dart';
import './tabs/home.dart';
import './tabs/category.dart';
import './tabs/settings.dart';
import './tabs/profile.dart';
import './tabs/message.dart';

class Tabs extends StatefulWidget {
  const Tabs({super.key});

  @override
  State<Tabs> createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  int currentIndex = 0;
  Color color = Colors.green;
  List<Widget> tabList = [
    const HomePage(),
    const CategoryPage(),
    const MessagePage(),
    const SettingsPage(),
    const ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("自定义底部导航"),
      ),
      body: tabList[currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        // 选中的颜色
        fixedColor: Colors.green,
        // 图标大小
        iconSize: 35,
        type: BottomNavigationBarType.fixed,
        // 菜单索引
        currentIndex: currentIndex,
        // 点击事件
        onTap: (index) {
          setState(() {
            currentIndex = index;
            if (index == 2) {
              color = Colors.red;
            }
          });
        },
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home_outlined), label: "首页"),
          BottomNavigationBarItem(
              icon: Icon(Icons.category_sharp), label: "分类"),
          BottomNavigationBarItem(icon: Icon(Icons.memory_sharp), label: "消息"),
          BottomNavigationBarItem(icon: Icon(Icons.settings), label: "设置"),
          BottomNavigationBarItem(icon: Icon(Icons.thumb_up), label: "我的"),
        ],
      ),
      // 浮动按钮
      floatingActionButton: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.only(bottom: 6),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(45)),
        width: 90,
        height: 90,
        child: FloatingActionButton(
          backgroundColor: color,
          onPressed: () {
            setState(() {
              currentIndex = 2;
            });
          },
          child: const Icon(Icons.add),
        ),
      ),
      // 浮动按钮位置
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      // 左侧抽屉组件
      drawer: Drawer(
          child: Column(
        children: [
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: UserAccountsDrawerHeader(
                    arrowColor: Colors.blue,
                      decoration: const BoxDecoration(color: Colors.pinkAccent),
                      currentAccountPicture: const CircleAvatar(
                        backgroundImage: NetworkImage(
                            "https://fanzhengxiang.oss-cn-chengdu.aliyuncs.com/kikiblog/blog-info-head.jpeg"),
                      ),
                      otherAccountsPictures: [
                        const CircleAvatar(backgroundImage: NetworkImage("https://www.itying.com/images/flutter/1.png"),),
                        const CircleAvatar(backgroundImage: NetworkImage("https://www.itying.com/images/flutter/2.png"),),
                        Image.network(
                          "https://www.itying.com/images/flutter/3.png",
                          fit: BoxFit.cover,
                        ),



                      ],
                      accountName: Text("帽子反戴-单手炒菜"),
                      accountEmail: Text("fanzhengxiangxx@gmail.com")))
            ],
          ),
          ListTile(
            leading: CircleAvatar(
              child: Icon(Icons.people),
            ),
            title: Text("个人中心"),
          ),
          Divider(),
          ListTile(
            leading: CircleAvatar(
              child: Icon(Icons.settings_applications_outlined),
            ),
            title: Text("系统设置"),
          ),
          Divider(),
        ],
      )),
      endDrawer: const Drawer(
        child: Text("右侧侧边栏"),
      ),
    );
  }
}
