import 'package:flutter/material.dart';
import '../data/listData.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "网格布局1",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("网格布局2")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: GridView.count(
        // 一行显示多少个组件
        crossAxisCount: 4,
        children: const [
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
        ],
      ),
    );
  }
}
