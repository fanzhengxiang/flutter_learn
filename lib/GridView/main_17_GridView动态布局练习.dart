import 'package:flutter/material.dart';
import '../data/listData.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "网格布局1",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("网格布局2")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  List<Widget> _initGridViewData() {
    List<Widget> tempList = [];
    for (int i = 0; i < listData.length; i++) {
      var data = listData[i];
      tempList.add(Container(

          alignment: Alignment.center,

          decoration:  BoxDecoration(
            border: Border.all(color: Colors.black26),
            color: Colors.white,
          ),
          child: Column(
            children: [
              Image.network(
                data['imgUrl'],
                fit: BoxFit.cover,
              ),
              const SizedBox(height: 20,),
              Text(
                data['title'],
                style: const TextStyle(color: Colors.black, fontSize: 25),
              ),
            ],
          )));
    }
    return tempList.toList();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: GridView.count(
          padding: const EdgeInsets.all(10),
          // 水平间距
          crossAxisSpacing: 10,
          // 垂直组件间的间距
          mainAxisSpacing: 30,
          // 一行最大组件个数
          crossAxisCount: 2,
          // 宽高比
          childAspectRatio: 0.9,
          children: _initGridViewData()),
    );
  }
}
