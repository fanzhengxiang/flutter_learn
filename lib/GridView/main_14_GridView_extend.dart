import 'package:flutter/material.dart';
import '../data/listData.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "网格布局1",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("网格布局2")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: GridView.extent(
        // 横轴子元素的最大长度
        maxCrossAxisExtent: 80,
        children: const [
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
          Icon(
            Icons.access_alarm,
            size: 40,
          ),
        ],
      ),
    );
  }
}
