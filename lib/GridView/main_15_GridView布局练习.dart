import 'package:flutter/material.dart';
import '../data/listData.dart';

void main() {
  runApp(const MyApp());
}

// 加载本地图片
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "网格布局1",
        theme: ThemeData(primarySwatch: Colors.pink),
        home: Scaffold(
          appBar: AppBar(title: const Text("网格布局2")),
          body: const HomePage(),
        ));
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  List<Widget> _initGridViewData() {
    List<Widget> tempList = [];
    for (int i = 0; i < 10; i++) {
      tempList.add(Container(
        decoration: const BoxDecoration(
          color: Colors.green,
        ),
        alignment: Alignment.center,
        child: Text(
          "我是第$i个元素",
          style: const TextStyle(color: Colors.red, fontSize: 25),
        ),
      ));
    }
    return tempList.toList();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
      child: GridView.count(
          padding: const EdgeInsets.all(10),
          // 水平间距
          crossAxisSpacing: 10,
          // 垂直组件间的间距
          mainAxisSpacing: 20,
          // 一行最大组件个数
          crossAxisCount: 2,
          // 宽高比
          childAspectRatio: 1.3,
          children: _initGridViewData()),
    );
  }
}
