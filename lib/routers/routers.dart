import 'package:flutter/material.dart';
import '../pages/tabs.dart';
import '../pages/tabs/search.dart';
import '../pages/tabs/news.dart';
import '../pages/tabs/form.dart';

Map<String, WidgetBuilder> routes = {
  '/': (context) => const Tabs(),
  '/news': (context) => const NewsPage(aid: 120),
  '/search': (context) => const SearchPage(),
  '/form': (context,{arguments}) =>
      FormPage(arguments: arguments),
};



// 配置onGenerateRoute
var onGenerateRoute = (RouteSettings settings) {
  final String? name = settings.name;
  final Function? pageContentBuilder = routes[name];

  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      return MaterialPageRoute(builder: (context) =>
          pageContentBuilder(context, arguments: settings.arguments));
    } else {
      return MaterialPageRoute(
          builder: (context) => pageContentBuilder(context));
    }
  } else {
    return null;
  }
};
