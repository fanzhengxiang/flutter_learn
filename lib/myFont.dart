import 'package:flutter/material.dart';

class AiresFont {
  static const IconData wechat =
      IconData(0xe61c, fontFamily: "kiki", matchTextDirection: true);

  static const IconData alipay =
      IconData(0xe8e4, fontFamily: "kiki", matchTextDirection: true);

  static const IconData sony =
      IconData(0xe67e, fontFamily: "kiki", matchTextDirection: true);

  static const IconData location =
      IconData(0xe651, fontFamily: "aires", matchTextDirection: true);

  static const IconData adidas =
      IconData(0xe618, fontFamily: "aires", matchTextDirection: true);
}
